# go-glitch-me [![Build Status](https://travis-ci.org/hryniuk/go-glitch-me.svg?branch=master)](https://travis-ci.org/hryniuk/go-glitch-me)
Glitch art in Go.

### References

* http://www.glitchet.com/resources

package main

import (
	"fmt"
	"github.com/hryniuk/go-glitch-me/util/load"
	"github.com/hryniuk/go-glitch-me/util/save"
	"image"
	"image/draw"
	"os"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Usage:\n\t " + os.Args[0] + " <input_file> <output_file>")
		os.Exit(1)
	}
	inputFile := os.Args[1]
	outputFile := os.Args[2]

	img, err := load.Png(inputFile)
	if err != nil {
		fmt.Println(err)
	}

	m := image.NewRGBA(image.Rect(0, 0, img.Bounds().Dx(), img.Bounds().Dy()))
	draw.Draw(m, m.Bounds(), img, image.ZP, draw.Src)

	err = save.Png(outputFile, m)
	if err != nil {
		fmt.Println(err)
	}
}

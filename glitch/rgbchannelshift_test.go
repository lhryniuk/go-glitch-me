package glitch

import (
	// "bytes"
	"image"
	"image/color"
	"image/draw"
	// "math"
	"testing"
)

var colors = []color.Color{
	color.RGBA{0, 100, 200, 255},
	color.RGBA{33, 66, 99, 122},
	color.RGBA{20, 10, 30, 100},
	color.RGBA{60, 50, 40, 10},
	color.RGBA{70, 80, 90, 150},
	color.RGBA{150, 180, 210, 240},
}

func newTestImage(width, height int, imgColors []color.Color) draw.Image {
	img := image.NewRGBA(image.Rect(0, 0, width, height))
	if len(imgColors) != width*height {
		panic("Wrong number of colors")
	}
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			img.Set(x, y, imgColors[y*width+x])
		}
	}
	return img
}

func TestGenerateRandomArguments(t *testing.T) {
	xOffsets, yOffsets, sourceChannels, targetChannels := generateRandomArguments(
		5, 5, 2, true, true)

	if !(len(xOffsets) == len(yOffsets) &&
		len(yOffsets) == len(sourceChannels) &&
		len(sourceChannels) == len(targetChannels)) {
		t.Error("All arrays returned by generateRandomArguments should have the same len")
	}
}

func TestRgbChannelShift(t *testing.T) {
	assertEqualColors := func(expectedColors []color.Color, actualImg image.Image) {
		width, height := actualImg.Bounds().Dx(), actualImg.Bounds().Dy()
		for y := 0; y < height; y++ {
			for x := 0; x < width; x++ {
				if expectedColors[width*y+x] != actualImg.At(x, y) {
					t.Error("Image's colors doesn't match expected ones")
					t.Errorf("expected colors: %v", expectedColors)
					t.Errorf("actual image: %v", actualImg)
				}
			}
		}
	}

	testImg1 := newTestImage(2, 2, colors[:4])
	var err error
	_, err = RgbChannelShift(testImg1, []int{0, 1}, []int{0}, []uint8{0}, []uint8{0})
	if err == nil {
		t.Error("RgbChannelShift should return error on invalid arguments")
	}

	_, err = RgbChannelShift(testImg1, []int{0, 1}, []int{0, 1}, []uint8{0, 1}, []uint8{0})
	if err == nil {
		t.Error("RgbChannelShift should return error on invalid arguments")
	}

	_, err = RgbChannelShift(testImg1, []int{0}, []int{0}, []uint8{0, 1}, []uint8{0})
	if err == nil {
		t.Error("RgbChannelShift should return error on invalid arguments")
	}

	actualImg1, _ := RgbChannelShift(testImg1, []int{1}, []int{0}, []uint8{0}, []uint8{0})
	assertEqualColors(
		[]color.Color{
			color.RGBA{33, 100, 200, 255}, color.RGBA{0, 66, 99, 122},
			color.RGBA{60, 10, 30, 100}, color.RGBA{20, 50, 40, 10}},
		actualImg1)

	actualImg2, _ := RgbChannelShift(testImg1, []int{0}, []int{1}, []uint8{2}, []uint8{1})
	assertEqualColors(
		[]color.Color{
			color.RGBA{0, 30, 200, 255}, color.RGBA{33, 40, 99, 122},
			color.RGBA{20, 200, 30, 100}, color.RGBA{60, 99, 40, 10}},
		actualImg2)

	testImg2 := newTestImage(3, 2, colors[:6])
	actualImg3, _ := RgbChannelShift(testImg2, []int{0, 2}, []int{1, 2}, []uint8{2, 1}, []uint8{1, 2})
	assertEqualColors(
		[]color.Color{
			color.RGBA{0, 40, 210, 255}, color.RGBA{33, 90, 40, 122}, color.RGBA{20, 210, 90, 100},
			color.RGBA{60, 200, 30, 10}, color.RGBA{70, 99, 200, 150}, color.RGBA{150, 30, 99, 240}},
		actualImg3)
}

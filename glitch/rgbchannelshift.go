package glitch

import (
	"errors"
	"github.com/hryniuk/go-glitch-me/util"
	"image/color"
	"image/draw"
	"math/rand"
	"time"
)

const (
	red   uint8 = iota
	green       = iota
	blue        = iota
)

const channelsCount = 3

func RandomRgbChannelShift(
	img draw.Image,
	iterationsCount uint,
	shiftHorizontally,
	shiftVertically bool) (draw.Image, error) {

	if iterationsCount == 0 {
		return img, errors.New("Invalid argument, iterationsCount should be positive")
	}

	xOffsets, yOffsets, sourceChannels, targetChannels := generateRandomArguments(
		img.Bounds().Dx(), img.Bounds().Dy(), iterationsCount, shiftHorizontally, shiftVertically)

	return RgbChannelShift(img, xOffsets, yOffsets, sourceChannels, targetChannels)
}

func generateRandomArguments(
	imgWidth,
	imgHeight int,
	iterationsCount uint,
	shiftHorizontally,
	shiftVertically bool) ([]int, []int, []uint8, []uint8) {

	source := rand.NewSource(time.Now().UnixNano())
	randGen := rand.New(source)

	var sourceChannels = make([]uint8, iterationsCount)
	var targetChannels = make([]uint8, iterationsCount)
	var xOffsets = make([]int, iterationsCount)
	var yOffsets = make([]int, iterationsCount)

	for i := uint(0); i < iterationsCount; i++ {
		sourceChannels[i] = uint8(randGen.Intn(channelsCount))
		targetChannels[i] = uint8(randGen.Intn(channelsCount))

		if shiftHorizontally {
			xOffsets[i] = randGen.Intn(imgWidth)
		}
		if shiftVertically {
			yOffsets[i] = randGen.Intn(imgHeight)
		}
	}

	return xOffsets, yOffsets, sourceChannels, targetChannels
}

func RgbChannelShift(
	img draw.Image,
	xOffsets,
	yOffsets []int,
	sourceChannels,
	targetChannels []uint8) (draw.Image, error) {

	if !(len(xOffsets) == len(yOffsets) &&
		len(yOffsets) == len(sourceChannels) &&
		len(sourceChannels) == len(targetChannels)) {
		return img, errors.New("All input arrays should have the same len")
	}

	iterationsCount := len(sourceChannels)
	for i := 0; i < iterationsCount; i++ {
		img = singleRgbChannelShift(img, xOffsets[i], yOffsets[i], sourceChannels[i], targetChannels[i])
	}

	return img, nil
}

func singleRgbChannelShift(
	img draw.Image,
	xOffset,
	yOffset int,
	sourceChannel,
	targetChannel uint8) draw.Image {

	outputImg := util.CopyImage(img)

	for x := 0; x < img.Bounds().Dx(); x++ {
		for y := 0; y < img.Bounds().Dy(); y++ {
			shiftPixelColor(img, outputImg, x, y, xOffset, yOffset, sourceChannel, targetChannel)
		}
	}

	return outputImg
}

func shiftPixelColor(sourceImg, targetImg draw.Image, x, y, xOffset, yOffset int, sourceChannel, targetChannel uint8) {
	sourcePixel := sourceImg.At((x+xOffset)%sourceImg.Bounds().Dx(),
		(y+yOffset)%sourceImg.Bounds().Dy())
	sourceChannelValue := getSourceChannelValue(sourcePixel, sourceChannel)

	targetImg.Set(x, y, getTargetColor(sourceChannelValue, targetChannel, sourceImg.At(x, y)))
}

func getSourceChannelValue(sourceColor color.Color, sourceChannel uint8) uint8 {
	r, g, b, _ := sourceColor.RGBA()
	switch sourceChannel {
	case red:
		return uint8(r)
	case green:
		return uint8(g)
	case blue:
		return uint8(b)
	}
	return uint8(channelsCount)
}

func getTargetColor(sourceChannelValue, targetChannel uint8, targetColor color.Color) color.Color {
	r, g, b, a := targetColor.RGBA()
	switch targetChannel {
	case red:
		return color.RGBA{sourceChannelValue, uint8(g), uint8(b), uint8(a)}
	case green:
		return color.RGBA{uint8(r), sourceChannelValue, uint8(b), uint8(a)}
	case blue:
		return color.RGBA{uint8(r), uint8(g), sourceChannelValue, uint8(a)}
	}
	return targetColor
}

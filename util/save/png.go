package save

import (
    "os"
    "image"
    "image/png"
)

func Png(filepath string, img image.Image) error {
    out, err := os.Create(filepath)
    if err != nil {
        return err
    }

    err = png.Encode(out, img)
    if err != nil {
        return err
    }

    return nil
}

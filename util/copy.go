package util

import (
    "image"
    "image/draw"
)

func CopyImage(img image.Image) draw.Image {
    imgCopy := image.NewRGBA(image.Rect(0, 0, img.Bounds().Dx(), img.Bounds().Dy()))
    draw.Draw(imgCopy, img.Bounds(), img, image.ZP, draw.Src)
    return imgCopy
}

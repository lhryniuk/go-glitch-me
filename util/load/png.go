package load

import (
    "image"
    "image/png"
    "os"
)

func Png(filename string) (image.Image, error) {
    file, err := os.Open(filename)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    img, decErr := png.Decode(file)
    if decErr != nil {
        return nil, decErr
    }

    return img, err
}
